#ifndef __STRING_H__
#define __STRING_H__

extern int strcmp(const char *s1, const char *s2);

#endif // __STRING_H__
