#ifndef __UART_H__
#define __UART_H__


typedef volatile struct uart 
{
	char *base;
	int n;
} UART;

extern UART uart[4];

extern int uart_init();
extern int ugetc(UART *up);
extern int uputc(UART *up, char c);
extern int ugets(UART *up, char *s);
extern int uprints(UART *up, char *s);

#endif // __UART_H__

